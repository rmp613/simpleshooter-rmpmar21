﻿using UnityEngine;
using System.Collections;

//This script manages the player object
public class Player : Spaceship
{
    public int hp;          //Ship's hit points

    int currentHP;              //Ship's current hit points
    int keyCount = 0;
    float dodgeCooler;
    float dodgeCoolerDefault = 0.15f;
    float keyCooler;
    float keyCoolerDefault = 0.2f;
    bool dodging = false;
    int dodgeDir = 0; //dodge direction -1 == left 1 == right
    float defaultSpeed;

    override protected void OnEnable()
    {
        base.OnEnable();
        currentHP = hp;

        defaultSpeed = speed;
        keyCooler = keyCoolerDefault;
    }
    void CheckDodge()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (keyCooler > 0 && keyCount == 1)
            {
                dodging = true;
                dodgeDir = -1;
                dodgeCooler = dodgeCoolerDefault;
            }
            else
            {
                keyCooler = keyCoolerDefault;
                keyCount++;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (keyCooler > 0 && keyCount == 1)
            {
                dodging = true;
                dodgeDir = 1;
                dodgeCooler = dodgeCoolerDefault;
            }
            else
            {
                keyCooler = keyCoolerDefault;
                keyCount++;
            }

        }
        if (keyCooler > 0)
        {
            keyCooler -= 1 * Time.deltaTime;
        }
        else
        {
            keyCount = 0;
        }
    }
    void Dodge()
    {
        if (dodgeCooler <= 0)
        {
            dodging = false;
            speed = defaultSpeed;
        }
        if (dodging)
        {
            dodgeCooler -= 1 * Time.deltaTime;
            speed = defaultSpeed * 2.3f;
        }
    }
void Update()
{

    //Get our raw inputs
    float x = Input.GetAxisRaw("Horizontal");
    float y = Input.GetAxisRaw("Vertical");

    CheckDodge();
    Dodge();
    if (dodging) { x = dodgeDir; }


    //Normalize the inputs
    Vector2 direction = new Vector2(x, y).normalized;
    //Move the player
    Move(direction);
}

void Move(Vector2 direction)
{
    //Find the screen limits to the player's movement
    Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
    Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
    //Get the player's current position
    Vector2 pos = transform.position;
    //Calculate the proposed position
    Vector2 newPos = pos + direction * speed * Time.deltaTime;
    if (newPos.x > max.x || newPos.x < min.x || newPos.y > max.y || newPos.y < min.y)
    {
        newPos = pos;
    }
    //Update the player's position
    transform.position = newPos;
}

void OnTriggerEnter2D(Collider2D c)
{
    //Get the layer of the collided object
    string layerName = LayerMask.LayerToName(c.gameObject.layer);
    //If the player hit an enemy bullet
    if (layerName == "Bullet (Enemy)")
    {
        Bullet obj = c.GetComponent<Bullet>();
        //...return the bullet to the pool...
        ObjectPool.current.PoolObject(c.gameObject);
        currentHP -= obj.power;

        // play the damaged animation
        animator.SetTrigger("Damage");
        //if enemy set health to 0
    }
    else if (layerName == "Enemy")
    {
        currentHP = 0;
    }
    //if health is 0 or less explode etc
    if (currentHP <= 0)
    {
        //...call the parent Explode method...
        Explode();
        //Tell the manager that we crashed
        Manager.current.GameOver();
        //Trigger an explosion
        Explode();
        //Deactivate the player
        gameObject.SetActive(false);
    }
}
}